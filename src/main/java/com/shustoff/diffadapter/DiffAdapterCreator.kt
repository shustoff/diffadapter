package com.shustoff.diffadapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import kotlin.reflect.KClass

class DiffAdapterCreator internal constructor() {
    private val holderCreatorsRegistry = HashMap<Class<out Any>, HolderInfo>()
    private val typesRegistry = HashMap<Int, HolderInfo>()

    inline fun <reified T : Any> forType() = DiffAdapterTypeBinder(T::class)

    fun <T : Any> bindHolderInternal(
        clazz: KClass<T>,
        itemsCountInRow: Int = 1,
        createFromParent: (parent: ViewGroup) -> TypedViewHolder<T>
    ): DiffAdapterCreator {
        val holderInfo =
            HolderInfo(createFromParent, typesRegistry.size, itemsCountInRow)

        holderCreatorsRegistry[clazz.java] = holderInfo
        typesRegistry[holderInfo.viewType] = holderInfo
        return this
    }

    fun build(): DiffAdapter = DiffAdapter(holderCreatorsRegistry, typesRegistry)

    inner class DiffAdapterTypeBinder<T : Any>(
        private val clazz: KClass<T>
    ) {
        fun <Binding : ViewBinding> holder(
            inflateBinding: (LayoutInflater, ViewGroup, Boolean) -> Binding,
            itemsCountInRow: Int = 1,
            createHolder: (Binding) -> TypedViewHolder<T>
        ): DiffAdapterCreator {
            return bindHolderInternal(clazz, itemsCountInRow) { parent ->
                val binding =
                    inflateBinding.invoke(LayoutInflater.from(parent.context), parent, false)
                createHolder.invoke(binding)
            }
        }

        fun <Binding : ViewBinding> binding(
            inflateBinding: (LayoutInflater, ViewGroup, Boolean) -> Binding,
            onClick: (Binding.(T) -> Unit)? = null,
            init: Binding.() -> Unit = { },
            bind: Binding.(T) -> Unit = { },
            itemsCountInRow: Int = 1
        ): DiffAdapterCreator {
            return bindHolderInternal(clazz, itemsCountInRow) { parent ->
                val binding =
                    inflateBinding.invoke(LayoutInflater.from(parent.context), parent, false)
                init.invoke(binding)
                SimpleViewHolder(binding, onClick, bind)
            }
        }
    }
}

fun diffAdapterBuilder() = DiffAdapterCreator()