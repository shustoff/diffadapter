package com.shustoff.diffadapter

import android.view.ViewGroup

internal class HolderInfo(
    val createFromParent: (parent: ViewGroup) -> TypedViewHolder<*>,
    val viewType: Int,
    val itemsCountInRow: Int
)