package com.shustoff.diffadapter

import com.shustoff.diffadapter.utils.Differentiable

interface SingleInstanceInList : Differentiable {

    override fun isSame(other: Any): Boolean = this::class == other::class

    override fun isContentSame(other: Any): Boolean = this == other
}