package com.shustoff.diffadapter

import android.view.View

class StubViewHolder<T>(
    itemView: View,
    onClick: ((T) -> Unit)? = null
) : TypedViewHolder<T>(itemView, onClick) {

    override fun bind(item: T) {
    }
}