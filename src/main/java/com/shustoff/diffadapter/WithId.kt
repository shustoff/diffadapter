package com.shustoff.diffadapter

import com.shustoff.diffadapter.utils.Differentiable

interface WithId<T> : Differentiable {

    val id: T

    override fun isSame(other: Any): Boolean =
        javaClass == other.javaClass && other is WithId<*> && id == other.id

    override fun isContentSame(other: Any): Boolean = this == other
}
