package com.shustoff.diffadapter.utils

import androidx.recyclerview.widget.DiffUtil

internal class DiffCallback(
    private val oldItems: List<Any?>,
    private val newItems: List<Any?>
) : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldItems[oldItemPosition] ?: return false
        val newItem = newItems[newItemPosition] ?: return false

        return if (oldItem is Differentiable && newItem is Differentiable) {
            oldItem.isSame(newItem)
        } else {
            oldItem == newItem
        }
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldItems[oldItemPosition] ?: return false
        val newItem = newItems[newItemPosition] ?: return false

        return if (oldItem is Differentiable && newItem is Differentiable) {
            oldItem.isContentSame(newItem)
        } else {
            oldItem == newItem
        }
    }

    override fun getOldListSize() = oldItems.size

    override fun getNewListSize() = newItems.size
}